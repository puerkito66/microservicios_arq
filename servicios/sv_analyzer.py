# -*- coding: utf-8 -*-
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_information.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor(es): Orlando Rodríguez y Felipe Ulloa.
# Version: 1.0 Abril 2019
# Descripción:
#
#   Este archivo define el rol de un servicio. Su función general es porporcionar en un objeto JSON
#   los tweets más relevantes sobre un tópico específico.
#
#
#
#                                        sv_twitter.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |    Analizador de      |  - Realizar el análisis | - Utiliza la librería  |
#           |    comentarios en     |    de sentimientos de   |   Textblob             |
#           |       Twitter         |    los tweets obtenidos.| - Devuelve un puntaje  |
#           |                       |                         |   sobre la polaridad   |
#           |                       |                         |   los tweets.          |
#           +-----------------------+-------------------------+------------------------+
#
#   Ejemplo de uso: Abrir navegador e ingresar a http://localhost:8084/api/v1/analize enviando
#   los tweets a analizar en formato json por medio de POST.
#
import os
from flask import Flask, abort, render_template, request
import json
import random

from textblob import TextBlob

app = Flask(__name__)


@app.route("/api/v1/analize", methods=['POST'])
def get_information():
    """
    Este método ejecuta un análisis de sentimientos sobre tweets recabados sobre una
    película específica.
    :return: float representando la polaridad general de los tweets.
    """
    tweets = request.get_json()
    if not tweets:
        return abort(400)

    polarity = 0.0

    for t in tweets:
        blob = TextBlob(t)
        polarity+= blob.sentiment.polarity
        
    polarity = polarity / len(tweets)

    ran_tweets = []

    for t in random.sample(tweets, k=4):
        ran_tweets.append({'tweet':t, 'polaridad':TextBlob(t).sentiment.polarity})

    return json.dumps({'tweets':ran_tweets, 'puntaje':polarity}), 200


if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8090))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)