# -*- coding: utf-8 -*-
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_information.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor(es): Orlando Rodríguez y Felipe Ulloa.
# Version: 1.0 Abril 2019
# Descripción:
#
#   Este archivo define el rol de un servicio. Su función general es porporcionar en un objeto JSON
#   los tweets más relevantes sobre un tópico específico.
#
#
#
#                                        sv_twitter.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |    Procesador de      |  - Ofrecer un JSON que  | - Utiliza el API de    |
#           |    comentarios en     |    contenga tweets      |   Twitter.             |
#           |       Twitter         |    obtenidos desde      | - Devuelve un JSON con |
#           |                       |    Twitter.             |   tweets sobre un      |
#           |                       |                         |   tópico en específico.|
#           +-----------------------+-------------------------+------------------------+
#
#   Ejemplo de uso: Abrir navegador e ingresar a http://localhost:8084/api/v1/twitter?t=matrix
#
import os
from flask import Flask, abort, render_template, request
import json
import tweepy

app = Flask(__name__)


@app.route("/api/v1/twitter")
def get_information():
    """
    Este método obtiene información acerca de una película o serie
    específica.
    :return: JSON con la información de la película o serie
    """
    # Se lee el parámetro 't' que contiene el título de la película o serie que se va a consultar
    movie = request.args.get("m")
    
    # Twitter API credentials
    consumer_key = '8i70E2UKTUgRIjuB17SSxnSPH'
    consumer_secret = 'LIYPZ9ea7zg6A1AnLAxHk1YF3jYRfWFfgrQwZJB6mMMBSQnYmN'
    access_key = '575998614-NFfns74Oay8IDBlOdjCtf5tkgfv3LN7zOyYYt23P'
    access_secret = 'gVJYohjPkcmxHwf1DZcI5xlRpFAKkyMB2fFrcYkGhX9tA'

    # Create the api endpoint

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    api = tweepy.API(auth)


    maximum_number_of_tweets_to_be_extracted = 10

    hashtag = "".join(movie.split())

    tweets = []

    for tweet in tweepy.Cursor(api.search, q='#' + hashtag + \
        ' -filter:media -filter:retweets -filter:links', \
        rpp=100, lang='es', tweet_mode='extended').items(maximum_number_of_tweets_to_be_extracted):
        t = tweet.full_text.encode('utf-8')
        tweets.append(t)

    return json.dumps(tweets)


if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8085))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)